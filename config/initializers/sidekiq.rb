
require_relative '../../lib/middleware'

rconfig = {url: 'redis://127.0.0.1:6379', middlewares: [NamespaceMiddleware]}

Sidekiq.configure_client do |config|
  config.redis = rconfig
end

Sidekiq.configure_server do |config|
  config.redis = rconfig
end

