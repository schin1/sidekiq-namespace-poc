require 'pry'
require 'redis'
require 'redis-namespace'
require 'redis_client'
require 'sidekiq'

require_relative '../../app/sidekiq/hard_job'
require_relative '../middleware'

desc 'Say hello!'
task :generate_load do
  rconfig = {url: 'redis://127.0.0.1:6379', middlewares: [NamespaceMiddleware]}

  Sidekiq.configure_client do |config|
    config.redis = rconfig
  end

  loop do
    HardJob.perform_async
    HardJob.perform_at(5)
    sleep(3)
  end
end
