require 'redis-namespace'


# NamespaceMiddleware can be injected into a RedisClient
# and introduce namespaces
module NamespaceMiddleware
  NAMESPACE = "resque:gitlab".freeze

  # add support for BITFIELD
  # https://github.com/mperham/sidekiq/blob/main/lib/sidekiq/metrics/shared.rb#L15
  COMMANDS = Redis::Namespace::NAMESPACED_COMMANDS.merge({'bitfield' => [:first] })
  
  def connect(redis_config)
    super
  end

  def call(command, redis_config)
    command = update_arguments(command)
    
    result = super
    result = update_result(result) if command.first == 'BRPOP'
    
    result
  end

  def call_pipelined(commands, redis_config)
    commands.each do |command| 
      next if command.first == "HELLO"
      command = update_arguments(command)
    end

    results = super

    results.each_with_index do |result, i|
      # the only command which requires modification to result
      # used by Sidekiq is brpop
      result = update_result(result) if commands[i].first == 'BRPOP'
    end

    results
  end

  def update_arguments(args)
    before, after = COMMANDS[args.first.downcase]

    command = args.shift

    case before
    when :first
      args[0] = add_namespace(args[0]) if args[0]
      args[-1] = ruby2_keywords_hash(args[-1]) if args[-1].is_a?(Hash)
    when :all
      args = add_namespace(args)
    when :eval_style
      # simplified to fit https://github.com/mperham/sidekiq/blob/main/lib/sidekiq/scheduled.rb#L57
      keysize = args[1].to_i
      args[2..2+keysize-1] = add_namespace(args[2..2+keysize-1])
    when :exclude_last
      last = args.pop unless args.length == 1
      args = add_namespace(args)
      args.push(last) if last
    end
    args.unshift(command)
  end

  def update_result(result)
    result[0] = rem_namespace(result[0]) if result
    result
  end

  # Redis::Namespace private method
  def add_namespace(key)
    case key
    when Array
      key.map! {|k| add_namespace k}
    when Hash
      key.keys.each {|k| key[add_namespace(k)] = key.delete(k)}
      key
    else
      "#{NAMESPACE}:#{key}"
    end
  end

  # Redis::Namespace private method
  def rem_namespace(key)
    case key
    when Array
      key.map {|k| rem_namespace k}
    when Hash
      Hash[*key.map {|k, v| [ rem_namespace(k), v ]}.flatten]
    when Enumerator
      create_enumerator do |yielder|
        key.each { |k| yielder.yield rem_namespace(k) }
      end
    else
      key.to_s.sub(/\A#{NAMESPACE}:/, '')
    end
  end
end
