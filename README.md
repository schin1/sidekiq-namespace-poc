# README

This project sets up a sidekiq client and server as a POC for introducing a namespace middleware into Sidekiq via a RedisClient middleware.

This allows us to use namespaces in Sidekiq 7.0 which dropped support for redis-namespace.

See lib/middleware.rb for middleware implementation details.

## Installation

This project assumes you have Ruby, Redis, and bundler installed.

To install the project, run
```
bundle install
```

## Getting started

Start a redis server on port 6379 via:

```
redis-server
```

To start the queue consumer, run:
```
# run server
bundle exec sidekiq -e production

# example
➜  sidekiq-namespace-poc bundle exec sidekiq -e production
2023-01-20T04:24:44.457Z pid=63099 tid=1drz WARN: You are running SQLite in production, this is generally not recommended. You can disable this warning by setting "config.active_record.sqlite3_production_warning=false".
2023-01-20T04:24:44.580Z pid=63099 tid=1drz INFO: Booted Rails 7.0.4.1 application in production environment
2023-01-20T04:24:44.580Z pid=63099 tid=1drz INFO: Running in ruby 3.1.3p185 (2022-11-24 revision 1a6b16756e) [arm64-darwin22]
2023-01-20T04:24:44.580Z pid=63099 tid=1drz INFO: See LICENSE and the LGPL-3.0 for licensing details.
2023-01-20T04:24:44.580Z pid=63099 tid=1drz INFO: Upgrade to Sidekiq Pro for more features and support: https://sidekiq.org
2023-01-20T04:24:44.580Z pid=63099 tid=1drz INFO: Sidekiq 7.0.3 connecting to Redis with options {:size=>5, :pool_name=>"internal", :url=>"redis://127.0.0.1:6379", :middlewares=>[NamespaceMiddleware]}
2023-01-20T04:24:44.582Z pid=63099 tid=1drz INFO: Sidekiq 7.0.3 connecting to Redis with options {:size=>5, :pool_name=>"default", :url=>"redis://127.0.0.1:6379", :middlewares=>[NamespaceMiddleware]}
2023-01-20T04:24:44.588Z pid=63099 tid=wfz class=HardJob jid=35de0b7ca40b58f1b21bd921 INFO: start
```

To generate load for Sidekiq, run:
```
# run client
bundle exec rake generate_load

# example
➜  sidekiq-namespace-poc bundle exec rake generate_load
2023-01-20T04:24:29.519Z pid=62876 tid=1di8 INFO: Sidekiq 7.0.3 connecting to Redis with options {:size=>5, :pool_name=>"internal", :url=>"redis://127.0.0.1:6379", :middlewares=>[NamespaceMiddleware]}
```

Redis keys should be namespaced with `resque:gitlab`:

```
127.0.0.1:6379> keys *
 1) "resque:gitlab:j|20230120|4:25"
 2) "resque:gitlab:j|20230120|4:24"
 3) "resque:gitlab:HardJob-20-04:24"
 4) "resque:gitlab:schedule"
 5) "resque:gitlab:queues"
 6) "resque:gitlab:HardJob-20-04:25"
 7) "resque:gitlab:Sylvesters-MacBook-Pro-2.local:63099:6b24fda7c504"
 8) "resque:gitlab:stat:processed"
 9) "resque:gitlab:stat:failed:2023-01-20"
10) "resque:gitlab:stat:processed:2023-01-20"
11) "resque:gitlab:stat:failed"
```
